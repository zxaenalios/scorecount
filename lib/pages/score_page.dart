import 'package:flutter/material.dart';

class Scores extends StatelessWidget {
  final String receivedData1;
  final String receivedData2;
  Scores(this.receivedData1, this.receivedData2);

  @override
      Widget build(BuildContext context) {
        return MaterialApp(
          home: Score(
            rd1: receivedData1,
            rd2: receivedData2,
          ),
        );
      }
    }



class Score extends StatefulWidget {
  Score({Key key, this.rd1, this.rd2}) : super(key: key);
  final String rd1;
  final String rd2;

  // final String receivedData1;
  // final String receivedData2;
  // Score(this.receivedData1, this.receivedData2);

  @override
  _Score createState() => _Score();  
  
}

class _Score extends State<Score> {

  int _score1 = 0;
  int _score2 = 0;

  void add1() {
    setState(() {
      _score1++;
    });
  }

  void add2() {
    setState(() {
      _score2++;
    });
  }

  void minus1() {
    setState(() {
      _score1--;
    });
  }

  void minus2() {
    setState(() {
      _score2--;
    });
  }

  void reset() {
    setState(() {
      _score1=0;
      _score2=0;
    });
  }

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Score Screen"),
      ),
      body: Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      shape: BoxShape.rectangle,
                    ), 
                    child: Text('$_score1',
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 70, fontWeight: FontWeight.bold, color: Colors.white,)),
                  ),
                  Container(
                    width: 150,
                    height: 150,
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      shape: BoxShape.rectangle,
                    ), 
                    child: Text('$_score2',
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 70, fontWeight: FontWeight.bold, color: Colors.white,)),
                  ),
                  
                ]
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                        width: 150,
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(16),
                        child: Text(widget.rd1,
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                      ),

                  Container(
                    width: 150,
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(16),
                    child: Text(widget.rd2,
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
              Row(
                
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                        width: 150,
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(16),
                        child: 
                  FloatingActionButton(
                    heroTag: "btn1",
                    onPressed: add1,
                    child: new Icon(Icons.add, color: Colors.white,),
                    backgroundColor: Colors.green
                  ),),
                  Container(
                        width: 150,
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(16),
                        child: 
                  FloatingActionButton(
                    heroTag: "btn2",
                    onPressed: add2,
                    child: new Icon(Icons.add, color: Colors.white,),
                    backgroundColor: Colors.green
                  ),)
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                        width: 150,
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(16),
                        child: 
                  FloatingActionButton(
                    heroTag: "btn3",
                    onPressed: minus1,
                    child: new Icon(
                    const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                      color: Colors.white),
                    backgroundColor: Colors.red,
                  ),),
                  Container(
                        width: 150,
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(16),
                        child: 
                  FloatingActionButton(
                    heroTag: "btn4",
                    onPressed: minus2,
                    child: new Icon(
                    const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                      color: Colors.white),
                    backgroundColor: Colors.red,
                  ),),
                      
                
                ],
              ),

             
              Container(
                width: 320,
                height: 40,
                margin: EdgeInsets.all(16),
                
                child:
              
              RaisedButton(        
                color: Colors.grey,
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                onPressed: reset,
                child: Text('Reset'),
              ),)
            ],
      ),
    ));
  }
}

