import 'package:flutter/material.dart';
import 'score_page.dart';

class Input extends StatefulWidget {
  @override
  _Input createState() => _Input();
}

class _Input extends State<Input> {
  TextEditingController _team1;
  TextEditingController _team2;

  void initState() {
    super.initState();
    _team1 = TextEditingController();
    _team2 = TextEditingController();
    _team1.text = "";
    _team2.text = "";
  }

  void dispose() {
    _team1.dispose();
    _team2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Score Count")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Team 1", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Container(
                width: 320,
                height: 40,
                margin: EdgeInsets.all(10),
                
                child:
              TextField(
                
                textAlign: TextAlign.center,
                controller: _team1,
                onSubmitted: (text) {
                  setState(() {});
                },
              ),),
              Text("Team 2", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              Container(
                width: 320,
                height: 40,
                margin: EdgeInsets.all(10),
                
                child:
             
              TextField(
                textAlign: TextAlign.center,
                controller: _team2,
                onSubmitted: (text) {
                  setState(() {});
                },
              ),),
              
              Container(
                width: 320,
                height: 40,
                margin: EdgeInsets.all(16),
                
                child:
              RaisedButton(        
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                color: Colors.blue,
                child: Text('Start'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Scores(_team1.text, _team2.text ),
                    ),
                  );
                },
              ),)
            ],
          ),
        ));
  }
}
